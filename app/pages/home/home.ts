// <reference path="../../custom/variables.ts" />
import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import {ModalController} from 'ionic-angular';
import {LoadingController} from 'ionic-angular';
import {Keyboard} from 'ionic-native';


//import {variables} from 'build/custom/custom/variables';
import {variables} from '../../custom/variables';
import {mainPopover} from '../../custom/popover';
import {dataCalls} from '../../custom/dataCalls';


@Component({
  providers: [dataCalls],
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  private loader;

  private search: string = '';
  private items: string[];
  private allItems: any;
  private popItem: any;
  private data: any;
  private theSources: any;
  private theNetflix: any;
  private loadingItem = [{
      title: "Loading...",
      desc: "",
      img: "build/images/film.png",
      year: "Loading...",
      type: "Loading...",
      imdb: "N/A",
      rating: "N/A",
      link: false
    }];
  private errorItem = [{
    title: "Error...try searching again",
    desc: "",
    img: "build/images/film.png",
    year: "There was an error searching!",
    type: "Error...",
    imdb: "N/A",
    rating: "N/A",
    link: false
  }];


  constructor(private alertController: AlertController, private navCtrl: NavController, private modalController: ModalController, private loadingController: LoadingController, public dC: dataCalls){
    this.initializeItems();

  }

  public vara: variables = new variables;

  initializeItems(){
    this.allItems = [
      {
        title: "Enter a Search Query!",
        desc: "Search must be 2 characters or more",
        img: "build/images/film.png",
        year: "Enter a query",
        type: "Enter a search",
        imdb: "N/A",
        rating: "N/A",
        link: false
      }
    ]
  }

  ionViewLoaded(){

  }

  ionViewWillEnter(){
    console.log("fired willenter");
  }

  fireAlert(val: string){
    let alM = this.vara.genOK('saved');
    let alt = this.alertController.create(alM);
    alt.present();
  }



  onInput(ev: any) {
    if (ev.target.value){
    this.search = ev.target.value;
    if (this.search.length >= 2){
      //this.fireAlert(this.search);
      this.allItems = this.loadingItem;
      this.dC.getInstant(this.search)
      .subscribe(data => {
          this.data = data;
          //console.dir(data);
          this.createArray();
        }, error => {
          console.log('error httpcall');
          this.allItems = this.errorItem;
      });
    } else {
      this.initializeItems();
    }
  } else {
    Keyboard.close();
    this.initializeItems();
  }
  }


  generatePopover() {
    var tItem;
    var tSources;
    var tNetflix;
    var error = 0;

    try {
      tItem = this.popItem;
      tSources = this.theSources;
      tNetflix = this.theNetflix;
    } catch (ex) {
      console.error(ex);
      this.allItems = this.errorItem;
      error = 1;
    }

    if (error === 0) {
      if (tSources._body.toString().replace(/(\s|\r|\n|\0|\t)/gmi, "") == "") {
        tSources._body = "<b>No Sources Found </b>" ;
      }

      this.loader.dismiss();
      console.log("should be dismissed");
      setTimeout(() => {
        let popov = this.modalController.create(mainPopover, {item: tItem, sources: tSources, netflix: tNetflix}, {showBackdrop: true});
        popov.present();
      }, 500);


    } else {
      this.loader.dismiss();
      this.allItems = this.errorItem;
    }
  }


  dataForPopover(id){
    let tRes = this.allItems;

    for (var i in tRes) {
      let ttRes = tRes[i];
      if (ttRes.imdb === id){
        this.popItem = ttRes;
        this.dataCallsPop();
        console.log(ttRes.type);
      }
    }
  }

  dataCallsPop(){
    let ttRes = this.popItem;
    if (ttRes.id !== null){
      this.dC.getSources(ttRes.type, ttRes.imdb)
        .subscribe(data => {
          this.theSources = data;
          console.dir(this.popItem);
          console.dir(this.theSources);
          this.dC.getNetflix(ttRes.title)
            .subscribe(data => {
              this.theNetflix = data;
              console.dir(this.theNetflix);

              this.generatePopover()
            }, error => {
              console.error(error);
			  this.loader.dismiss();
              this.allItems = this.errorItem;
            });
        }, error => {
          console.error(error);
		  this.loader.dismiss();
          this.allItems = this.errorItem;
      });
    }
  }





  onClick(link: boolean, imdb: any) {
    if (link){

      let loader = this.loadingController.create({
        content: "Loading title..."
      });
      this.loader = loader;
      this.loader.present();
      this.dataForPopover(imdb);

    }
  }
  createArray(){
    var tempArray = [];
    var dArray = this.data;
    for (var i in dArray.results) {
      console.dir(dArray.results[i]);
      var tRes = dArray.results[i];
      var tType = tRes.media_type;
      if (tType === "movie" || tType === "tv") {
        if (tType === "movie") {
          var tTitle = tRes.title;
          if (tRes.release_date !== null) {
            var tYear: string = "Release Date: " + tRes.release_date;
          } else {
            var tYear: string = " ";
          }
        } if (tType === "tv") {
          var tTitle = tRes.name;
          if (tRes.first_air_date !== null) {
            var tYear: string = "First Air Date: " + tRes.first_air_date;
          } else {
            var tYear: string = " ";
          }
        }
        var tDesc = tRes.overview;

        var tImg = "build/images/film.png";
        if (tRes.poster_path) {
          tImg = "http://www.what.fyi/img.php?url=http://image.tmdb.org/t/p/w500" + tRes.poster_path;
        }

        var tIMDB = tRes.id;

        if (tRes.vote_count > 0){
          var tRating = tRes.vote_average + "/10 ("+tRes.vote_count+ " reviews)";
        } else {
          var tRating = "0/10 (0 Reviews)";
        }


        var tItem = {
          title: tTitle,
          desc: tDesc,
          img: tImg,
          year: tYear,
          type: tType,
          imdb: tIMDB,
          rating: tRating,
          link: true
        };
        tempArray.push(tItem);
      }
    }
    this.allItems = tempArray;



  }

}
