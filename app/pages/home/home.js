"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// <reference path="../../custom/variables.ts" />
var core_1 = require('@angular/core');
var ionic_native_1 = require('ionic-native');
//import {variables} from 'build/custom/custom/variables';
var variables_1 = require('../../custom/variables');
var popover_1 = require('../../custom/popover');
var dataCalls_1 = require('../../custom/dataCalls');
var HomePage = (function () {
    function HomePage(alertController, navCtrl, modalController, loadingController, dC) {
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.dC = dC;
        this.search = '';
        this.loadingItem = [{
                title: "Loading...",
                desc: "",
                img: "build/images/film.png",
                year: "Loading...",
                type: "Loading...",
                imdb: "N/A",
                rating: "N/A",
                link: false
            }];
        this.errorItem = [{
                title: "Error...try searching again",
                desc: "",
                img: "build/images/film.png",
                year: "There was an error searching!",
                type: "Error...",
                imdb: "N/A",
                rating: "N/A",
                link: false
            }];
        this.vara = new variables_1.variables;
        this.initializeItems();
    }
    HomePage.prototype.initializeItems = function () {
        this.allItems = [
            {
                title: "Enter a Search Query!",
                desc: "Search must be 2 characters or more",
                img: "build/images/film.png",
                year: "Enter a query",
                type: "Enter a search",
                imdb: "N/A",
                rating: "N/A",
                link: false
            }
        ];
    };
    HomePage.prototype.ionViewLoaded = function () {
    };
    HomePage.prototype.ionViewWillEnter = function () {
        console.log("fired willenter");
    };
    HomePage.prototype.fireAlert = function (val) {
        var alM = this.vara.genOK('saved');
        var alt = this.alertController.create(alM);
        alt.present();
    };
    HomePage.prototype.onInput = function (ev) {
        var _this = this;
        if (ev.target.value) {
            this.search = ev.target.value;
            if (this.search.length >= 2) {
                //this.fireAlert(this.search);
                this.allItems = this.loadingItem;
                this.dC.getInstant(this.search)
                    .subscribe(function (data) {
                    _this.data = data;
                    //console.dir(data);
                    _this.createArray();
                }, function (error) {
                    console.log('error httpcall');
                    _this.allItems = _this.errorItem;
                });
            }
            else {
                this.initializeItems();
            }
        }
        else {
            ionic_native_1.Keyboard.close();
            this.initializeItems();
        }
    };
    HomePage.prototype.generatePopover = function () {
        var _this = this;
        var tItem;
        var tSources;
        var tNetflix;
        var error = 0;
        try {
            tItem = this.popItem;
            tSources = this.theSources;
            tNetflix = this.theNetflix;
        }
        catch (ex) {
            console.error(ex);
            this.allItems = this.errorItem;
            error = 1;
        }
        if (error === 0) {
            if (tSources._body.toString().replace(/(\s|\r|\n|\0|\t)/gmi, "") == "") {
                tSources._body = "<b>No Sources Found </b>";
            }
            this.loader.dismiss();
            console.log("should be dismissed");
            setTimeout(function () {
                var popov = _this.modalController.create(popover_1.mainPopover, { item: tItem, sources: tSources, netflix: tNetflix }, { showBackdrop: true });
                popov.present();
            }, 500);
        }
        else {
            this.loader.dismiss();
            this.allItems = this.errorItem;
        }
    };
    HomePage.prototype.dataForPopover = function (id) {
        var tRes = this.allItems;
        for (var i in tRes) {
            var ttRes = tRes[i];
            if (ttRes.imdb === id) {
                this.popItem = ttRes;
                this.dataCallsPop();
                console.log(ttRes.type);
            }
        }
    };
    HomePage.prototype.dataCallsPop = function () {
        var _this = this;
        var ttRes = this.popItem;
        if (ttRes.id !== null) {
            this.dC.getSources(ttRes.type, ttRes.imdb)
                .subscribe(function (data) {
                _this.theSources = data;
                console.dir(_this.popItem);
                console.dir(_this.theSources);
                _this.dC.getNetflix(ttRes.title)
                    .subscribe(function (data) {
                    _this.theNetflix = data;
                    console.dir(_this.theNetflix);
                    _this.generatePopover();
                }, function (error) {
                    console.error(error);
                    _this.loader.dismiss();
                    _this.allItems = _this.errorItem;
                });
            }, function (error) {
                console.error(error);
                _this.loader.dismiss();
                _this.allItems = _this.errorItem;
            });
        }
    };
    HomePage.prototype.onClick = function (link, imdb) {
        if (link) {
            var loader = this.loadingController.create({
                content: "Loading title..."
            });
            this.loader = loader;
            this.loader.present();
            this.dataForPopover(imdb);
        }
    };
    HomePage.prototype.createArray = function () {
        var tempArray = [];
        var dArray = this.data;
        for (var i in dArray.results) {
            console.dir(dArray.results[i]);
            var tRes = dArray.results[i];
            var tType = tRes.media_type;
            if (tType === "movie" || tType === "tv") {
                if (tType === "movie") {
                    var tTitle = tRes.title;
                    if (tRes.release_date !== null) {
                        var tYear = "Release Date: " + tRes.release_date;
                    }
                    else {
                        var tYear = " ";
                    }
                }
                if (tType === "tv") {
                    var tTitle = tRes.name;
                    if (tRes.first_air_date !== null) {
                        var tYear = "First Air Date: " + tRes.first_air_date;
                    }
                    else {
                        var tYear = " ";
                    }
                }
                var tDesc = tRes.overview;
                var tImg = "build/images/film.png";
                if (tRes.poster_path) {
                    tImg = "http://www.what.fyi/img.php?url=http://image.tmdb.org/t/p/w500" + tRes.poster_path;
                }
                var tIMDB = tRes.id;
                if (tRes.vote_count > 0) {
                    var tRating = tRes.vote_average + "/10 (" + tRes.vote_count + " reviews)";
                }
                else {
                    var tRating = "0/10 (0 Reviews)";
                }
                var tItem = {
                    title: tTitle,
                    desc: tDesc,
                    img: tImg,
                    year: tYear,
                    type: tType,
                    imdb: tIMDB,
                    rating: tRating,
                    link: true
                };
                tempArray.push(tItem);
            }
        }
        this.allItems = tempArray;
    };
    HomePage = __decorate([
        core_1.Component({
            providers: [dataCalls_1.dataCalls],
            templateUrl: 'build/pages/home/home.html'
        })
    ], HomePage);
    return HomePage;
}());
exports.HomePage = HomePage;
//# sourceMappingURL=home.js.map