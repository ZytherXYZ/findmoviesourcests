import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  templateUrl: 'build/pages/saved/saved.html'
})
export class SavedPage {
  constructor(private navCtrl: NavController) {
  }
}
