/**
 * Created by alecg on 8/16/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var dataCalls_1 = require("./dataCalls");
var variables_1 = require("./variables");
var mainPopover = (function () {
    function mainPopover(viewController, params, dC, aC) {
        this.viewController = viewController;
        this.params = params;
        this.dC = dC;
        this.aC = aC;
        this.vara = new variables_1.variables();
        this.sItem = this.params.data.item;
        this.sSources = this.params.data.sources;
        this.sNetflix = this.params.data.netflix;
    }
    mainPopover.prototype.ionViewLoaded = function () {
    };
    mainPopover.prototype.ionViewWillUnload = function () {
        console.log("Popover about to be destroyed!");
    };
    mainPopover.prototype.close = function () {
        this.viewController.dismiss();
    };
    mainPopover.prototype.saveItem = function () {
        var tMessage = this.vara.genOK("notsupported");
        var tAlert = this.aC.create(tMessage);
        tAlert.present();
    };
    mainPopover = __decorate([
        core_1.Component({
            providers: [dataCalls_1.dataCalls],
            template: "\n    <ion-modal-view>\n    <ion-header>\n    <ion-navbar>\n    <ion-buttons start>\n      <button (click)=\"close()\">\n        <ion-icon name=\"arrow-back\"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-title>{{sItem.title}}</ion-title>\n    <ion-buttons end>\n      <button (click)=\"saveItem()\">\n        <ion-icon name=\"add-circle\"></ion-icon>\n      </button>\n    </ion-buttons>\n    </ion-navbar>\n    </ion-header>\n    <ion-content>\n    <div id=\"sDiv\"><img id=\"sImg\" src=\"{{sItem.img}}\" /></div>\n    <ion-card>\n      <ion-card-header>\n        Description\n      </ion-card-header>\n      <ion-card-content>\n        {{sItem.desc}}\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <ion-card-header>\n        Sources\n      </ion-card-header>\n      <ion-card-content [innerHTML]=\"sSources._body\">\n\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <ion-card-header>\n        Netflix\n      </ion-card-header>\n      <ion-card-content [innerHTML]=\"sNetflix._body\">\n\n      </ion-card-content>\n    </ion-card>\n    </ion-content>\n    </ion-modal-view>\n  "
        })
    ], mainPopover);
    return mainPopover;
}());
exports.mainPopover = mainPopover;
//# sourceMappingURL=popover.js.map