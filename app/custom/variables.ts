/**
 * Created by alecg on 8/16/2016.
 */
export class variables {




  public genOK(tType: string) : any {
    var tTitle: string = "";
    var tBody: string = "";

    switch (tType) {
      case "saved":
            tTitle = "Saved!";
            tBody = "Entry has been saved!";
            break;
      case "deleted":
            tTitle = "Deleted!";
            tBody = "Deleted All Entries!";
            break;
      case "notsupported":
            tTitle = "Not Supported!";
            tBody = "Coming Soon! Sorry!";
            break;
      case "duplicate":
            tTitle = "Duplicate Detected!";
            tBody = "This title is already saved.";
            break;
      default:
            tTitle = "Error!";
            tBody = "An Unknown Error Occurred.";
            break;
    }
    var tThing: any = {
      title: tTitle,
      subTitle: tBody,
      buttons: ['OK']
    };
    return tThing;


  }
}
