"use strict";
/**
 * Created by alecg on 8/16/2016.
 */
var variables = (function () {
    function variables() {
    }
    variables.prototype.genOK = function (tType) {
        var tTitle = "";
        var tBody = "";
        switch (tType) {
            case "saved":
                tTitle = "Saved!";
                tBody = "Entry has been saved!";
                break;
            case "deleted":
                tTitle = "Deleted!";
                tBody = "Deleted All Entries!";
                break;
            case "notsupported":
                tTitle = "Not Supported!";
                tBody = "Coming Soon! Sorry!";
                break;
            case "duplicate":
                tTitle = "Duplicate Detected!";
                tBody = "This title is already saved.";
                break;
            default:
                tTitle = "Error!";
                tBody = "An Unknown Error Occurred.";
                break;
        }
        var tThing = {
            title: tTitle,
            subTitle: tBody,
            buttons: ['OK']
        };
        return tThing;
    };
    return variables;
}());
exports.variables = variables;
//# sourceMappingURL=variables.js.map