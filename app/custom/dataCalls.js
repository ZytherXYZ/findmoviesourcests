/**
 * Created by alecg on 8/16/2016.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/map');
var Observable_1 = require('rxjs/Observable');
var dataCalls = (function () {
    function dataCalls(http) {
        this.http = http;
        console.log("Http call init");
    }
    dataCalls.prototype.getInstant = function (query) {
        console.log(query);
        return this.http.post("http://www.findmoviesources.com/gMI?query=" + query, {})
            .map(function (res) { return res.json(); });
    };
    dataCalls.prototype.getSources = function (tvmov, id) {
        var tM = tvmov;
        if (tvmov === "movie") {
            tM = "mv";
        }
        console.log(tvmov + id);
        return this.http.post("http://www.findmoviesources.com/gMT?tvmov=" + tM + "&id=" + id, {})
            .map(function (res) { return res; });
    };
    dataCalls.prototype.getNetflix = function (query) {
        console.log(query);
        return this.http.post("http://www.findmoviesources.com/getFlix?query=" + query, {})
            .map(function (res) { return res; });
    };
    dataCalls.prototype.handleError = function (er) {
        console.error(er);
        return Observable_1.Observable.throw(er.json().error || 'error');
    };
    dataCalls = __decorate([
        core_1.Injectable()
    ], dataCalls);
    return dataCalls;
}());
exports.dataCalls = dataCalls;
//# sourceMappingURL=dataCalls.js.map