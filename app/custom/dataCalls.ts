/**
 * Created by alecg on 8/16/2016.
 */

import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';


@Injectable()

export class dataCalls {

  data: any;

  constructor(private http: Http) {
    console.log("Http call init");
  }

  getInstant(query: any){
    console.log(query);
    return this.http.post("http://www.findmoviesources.com/gMI?query=" + query, {})
      .map (res => res.json());
  }

  getSources(tvmov:any, id: any){
    var tM = tvmov;
    if (tvmov === "movie"){
      tM = "mv";
    }
    console.log(tvmov + id);
    return this.http.post("http://www.findmoviesources.com/gMT?tvmov="+tM+"&id="+id,{})
      .map (res => res);
  }
  getNetflix(query: any){
    console.log(query);
    return this.http.post("http://www.findmoviesources.com/getFlix?query="+query,{})
      .map(res => res);
  }

  handleError(er: Response){
    console.error(er);
    return Observable.throw(er.json().error || 'error');
  }
}
