/**
 * Created by alecg on 8/16/2016.
 */

import {Component} from '@angular/core';
import {ViewController} from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import {NavParams} from 'ionic-angular';
import {dataCalls} from "./dataCalls";
import {variables} from "./variables";

@Component({
  providers: [dataCalls],
  template: `
    <ion-modal-view>
    <ion-header>
    <ion-navbar>
    <ion-buttons start>
      <button (click)="close()">
        <ion-icon name="arrow-back"></ion-icon>
      </button>
    </ion-buttons>
    <ion-title>{{sItem.title}}</ion-title>
    <ion-buttons end>
      <button (click)="saveItem()">
        <ion-icon name="add-circle"></ion-icon>
      </button>
    </ion-buttons>
    </ion-navbar>
    </ion-header>
    <ion-content>
    <div id="sDiv"><img id="sImg" src="{{sItem.img}}" /></div>
    <ion-card>
      <ion-card-header>
        Description
      </ion-card-header>
      <ion-card-content>
        {{sItem.desc}}
      </ion-card-content>
    </ion-card>
    <ion-card>
      <ion-card-header>
        Sources
      </ion-card-header>
      <ion-card-content [innerHTML]="sSources._body">

      </ion-card-content>
    </ion-card>
    <ion-card>
      <ion-card-header>
        Netflix
      </ion-card-header>
      <ion-card-content [innerHTML]="sNetflix._body">

      </ion-card-content>
    </ion-card>
    </ion-content>
    </ion-modal-view>
  `
})


export class mainPopover {

  sItem: any;
  sSources: any;
  sNetflix: any;

  sTitle: string;
  sChange: string;
  sHTML: string;
  testHTML: string;
  sData: string;

  constructor(private viewController: ViewController, public params: NavParams, public dC: dataCalls, public aC: AlertController){
    this.sItem = this.params.data.item;
    this.sSources = this.params.data.sources;
    this.sNetflix = this.params.data.netflix;

  }
  private vara = new variables();


  ionViewLoaded(){

  }
  ionViewWillUnload(){
    console.log("Popover about to be destroyed!");
  }

  close(){
    this.viewController.dismiss();
  }

  saveItem(){
    let tMessage = this.vara.genOK("notsupported");
    let tAlert = this.aC.create(tMessage);
    tAlert.present();
  }

}
